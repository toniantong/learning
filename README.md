# Learning



## Getting started 開始使用

為了讓您輕鬆開始使用GitLab，這裡是一個推薦的下一步行動清單。

已經是專家了嗎？只需編輯此 README.md 文件並將其改為您自己的。想要使它變得容易嗎？[Use the template at the bottom 在底部使用模板。](#editing-this-readme)!

## Add your files

- [ ] [Create 創建](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) 或 [upload 上傳](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line 使用命令行添加文件。](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line)或使用以下命令推送現有的Git存儲庫：

```
cd existing_repo
git remote add origin https://gitlab.com/toniantong/learning.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools 與您的工具整合

- [ ] [Set up project integrations 設置專案整合](https://gitlab.com/toniantong/learning/-/settings/integrations)

## Collaborate with your team 與您的團隊合作

- [ ] [Invite team members and collaborators 邀請團隊成員和合作伙伴](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request 創建一個新的合併請求](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests 自動從合併請求中關閉問題](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals 啟用合併請求審核](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge 設定自動合併](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy 測試和部署

Use the built-in continuous integration in GitLab. 在GitLab中使用內建的持續整合。

- [ ] [Get started with GitLab CI/CD 開始使用GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST) 使用靜態應用程序安全測試（SAST）分析您的代碼中的已知漏洞。](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy 使用自動部署的方式將應用程式部署到Kubernetes、Amazon EC2或Amazon ECS](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management 使用拉取式部署以提升 Kubernetes 管理。](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments 建立受保護的環境](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README 編輯此自述文件

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). 當您準備好將此自述文件編輯成您自己的時候，只需編輯此文件，並使用下面的方便範本（或隨意結構化，您可根據自己的需求 - 這只是一個起點！）。Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.感謝 [makeareadme.com](https://www.makeareadme.com/) 提供這個範本。

## Suggestions for a good README 一份好的 README 建議

Every project is different, so consider which of these sections apply to yours.每個項目都不同，所以請考慮下面哪些部分適用於你的專案。The sections used in the template are suggestions for most open source projects.模板中使用的部分是為大多數開源專案提供的建議。Also keep in mind that while a README can be too long and detailed, too long is better than too short.同時請記住，儘管 README 可能太長且詳細，但太長總比太短好。If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.如果您認為您的README太長，請考慮使用其他形式的文檔，而不是剪掉資訊。

## Name 名稱
Choose a self-explaining name for your project.選擇一個自我解釋的名稱來命名你的專案。

## Description 描述
Let people know what your project can do specifically.讓人們明確知道你的項目能夠做什麼。Provide context and add a link to any reference visitors might be unfamiliar with.提供上下文並添加到可能不熟悉的參考鏈接。A list of Features or a Background subsection can also be added here.可以在這裡新增功能清單或背景子部分。If there are alternatives to your project, this is a good place to list differentiating factors.如果您的專案存在替代方案，這是一個好地方，可以列出區分因素。

## Badges 徽章
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project.在某些README中，您可能會看到一些小圖像，用於傳達元數據，例如專案中是否有所有測試都通過。You can use Shields to add some to your README.您可以使用「Shields」將其新增至您的 README 中。Many services also have instructions for adding a badge.很多服務也有關於添加徽章的說明。

## Visuals 視覺效果
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos).根據您製作的內容而定，包含截圖甚至影片（通常會更常見地使用GIF而不是實際影片）是一個不錯的主意。Tools like ttygif can help, but check out Asciinema for a more sophisticated method.利用ttygif等工具可以提供幫助，但請嘗試使用Asciinema來了解更高級的方法。

## Installation 安裝
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew.特定生態系統中，可能有一種常用的安裝方式，例如使用Yarn、NuGet或Homebrew。However, consider the possibility that whoever is reading your README is a novice and would like more guidance.然而，考慮到閱讀你的 README 的人可能是個新手，並且希望得到更多指導。Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible.列出具體步驟有助於消除不明確性，讓人們能盡快開始使用您的項目。If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.如果它只在特定的環境中運行，例如特定的程式語言版本或作業系統，或者需要手動安裝依賴項，則還需要新增一個要求（Requirements）子部分。

## Usage 使用方法
Use examples liberally, and show the expected output if you can.大方地使用例子，並且如果可以的話顯示預期的輸出。It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.在README中，最好能夠內嵌一個最簡單的使用示例，同時提供更複雜示例的鏈接，如果它們太長以至於無法合理地包含在其中。

## Support 支援
Tell people where they can go to for help.告訴人們可以去哪裡尋求幫助。It can be any combination of an issue tracker, a chat room, an email address, etc.可以是問題追蹤器、聊天室、電子郵件地址等的任意組合。

## Roadmap 路線圖
If you have ideas for releases in the future, it is a good idea to list them in the README.如果你有未來版本發布的想法，在README中列出它們是一個好主意。

## Contributing 貢獻
State if you are open to contributions and what your requirements are for accepting them.如果你願意接受貢獻，請說明並列出你的接受條件。

For people who want to make changes to your project, it's helpful to have some documentation on how to get started.對於想要對您的專案進行更改的人來說，擁有一些有關如何入門的文件是有幫助的。Perhaps there is a script that they should run or some environment variables that they need to set.也許有一個他們應該運行的腳本，或者一些他們需要設定的環境變數。Make these steps explicit.使這些步驟變得明確。These instructions could also be useful to your future self.這些指示對您未來的自己也可能很有用。

You can also document commands to lint the code or run tests.您也可以記錄用於檢查程式碼或執行測試的命令。These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something.您也可以記錄用於檢查代碼或執行測試的命令。Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.運行測試的指示對於需要外部設置的情況特別有幫助，例如在瀏覽器中測試時啟動Selenium服務器。

## Authors and acknowledgment 作者和致謝
Show your appreciation to those who have contributed to the project.對那些對項目做出貢獻的人表示感謝。

## License 準許
For open source projects, say how it is licensed.對於開源項目，說明其許可方式。

## Project status 項目狀態
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely.如果你已經耗盡了能量或時間來完成你的項目，請在 README 的頂部放置一張便籤，告知開發已經減緩或完全停止。Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going.有人可能選擇派生你的專案或自願接替成為維護者或所有者，讓你的專案得以繼續進行。You can also make an explicit request for maintainers.你也可以明確要求維護者。
