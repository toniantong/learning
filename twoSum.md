```java
/*
實戰練習: 兩數之和
有一個整數數組nums，能否從中取出兩個數，使它們的和為target。

輸入1：
nums = [4, 5, 7, 10]
target = 12

輸出1：
true


輸入2:
nums = [4, 5, 7, 10]
target = 8

輸出2:
false
*/
import java.util.*;

class twoSum {
    boolean FindTarget(int[] nums, int target) {
        for(int i = 0; i < nums.length; i++) {
            for(int j = i+1; j < nums.length;j++) {
                if(nums[i] + nums[j] == target) {
                    return true;
                }
            }
        }
        return false; // 如果沒有找到符合條件的數字對，則返回 false
    }
}
```
